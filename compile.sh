#!/bin/bash

HPCC=$PWD

echo "================================"
echo "--------------------------------"
icc -v >& /dev/null
if [ $? -eq 0 ]; then
	echo "Compiler is Intel"
	export COMPILER=intel
else
	echo "Compiler is GNU"
	export COMPILER=gnu
fi

# Check if MKL are present
echo "--------------------------------"
echo "Checking MKL library..."
if [ -n "$MKLROOT" ]; then
	# MKL are present
	echo "Using MKL library."
	CONFIG_LA="${MKLROOT}"
	CONFIG_LA_INCLUDE="-I${MKLROOT}/include"
	CONFIG_LA_LIB="-L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl"
	sed -i 's@\bCONFIG_LA\b@'"$CONFIG_LA"'@g' Make.Scife
	sed -i 's@\bCONFIG_LA_INCLUDE\b@'"$CONFIG_LA_INCLUDE"'@g' Make.Scife
	sed -i 's@\bCONFIG_LA_LIB\b@'"$CONFIG_LA_LIB"'@g' Make.Scife
else
	echo "Cannot find MKL, using BLAS."
	CONFIG_LA=""
	CONFIG_LA_INCLUDE=""
	CONFIG_LA_LIB=""
fi

# Move config
echo "--------------------------------"
echo "Generating configuration Make..."
cat Make.Scife
mv Make.Scife hpl/

# Make
echo "--------------------------------"
echo "Compiling..."
make arch=Scife || exit 1

echo "--------------------------------"
echo "DONE!"

echo "--------------------------------"
echo "================================"

exit 0
