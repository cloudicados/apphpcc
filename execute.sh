#!/bin/bash

HPCC=$PWD

echo "================================"
echo "--------------------------------"
icc -v >& /dev/null
if [ $? -eq 0 ]; then
	echo "Compiler is Intel"
	export COMPILER=intel
else
	echo "Compiler is GNU"
	export COMPILER=gnu
fi

# Execute
echo "--------------------------------"
echo "Execution begins..."
mpiexec -n [[[#TOTALCPUS]]] ./hpcc || {
	echo "HPCC failed with code: $?"
	exit 1
}

echo "--------------------------------"
echo "DONE!"

echo "--------------------------------"
echo "================================"

exit 0
