# AppHPCC #

This is an adapted version of the HPC Challenge [HPCC](https://icl.utk.edu/hpcc/) software v1.5.0.

## Usage and installation ##

* Download these files on a target folder.
* Use Scife to create the application with the contents of the target folder, e.g. using the `create_application` command of Scife's CLI. The compilation script must be set to `compile.sh` and the execution script must be set to `execute.sh`.
